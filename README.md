# Universe of Notes

Welcome to the universe of notes application.

## Work

Use the package manager [yarn](https://yarnpkg.com/getting-started/install) to work with universe-of-notes-app.

For development:

```bash
yarn dev
```

For production:

```bash
yarn built
```

For prettier formatting:

```bash
yarn prettier
```

For linter check:

```bash
yarn lint
```

For serving the production build:

- Install [serve](https://www.npmjs.com/package/serve) global.

```bash
yarn global add serve
```

- Serve the dist folder from root directory.

```bash
serve dist
```

## Authors

[Maksym Chernenko](https://www.linkedin.com/in/maksym-chernenko/)

[Maksym Mariukhin](https://www.linkedin.com/in/maksym-mariukhin/)
