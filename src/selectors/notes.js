import * as R from 'ramda';

import { ROOT } from 'Reducers/notes';

export const getSelectedNote = state => R.path([[ROOT], 'selectedNote'], state);

export const getNotes = state => R.path([[ROOT], 'notes'], state);

export const isNotesDisabled = state => R.path([[ROOT], 'isDisabled'], state);

export const getRecentlyDeletedNotes = state =>
  R.path([[ROOT], 'recentlyDeletedNotes'], state);

export const getLikes = state => R.path([[ROOT], 'likes'], state);

export const getUserNotes = state => R.path([[ROOT], 'userNotes'], state);
