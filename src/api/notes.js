import axios from 'axios';

const ROOT = '/notes';

export const fetchNotes = async ({ folderId }) => {
  const url = `${ROOT}?folderId=${folderId}`;

  const { data } = await axios.get(url);

  return data;
};

export const createNote = async ({ payload }) => {
  const url = ROOT;

  const { data } = await axios.post(url, payload);

  return data;
};

export const getNote = async ({ payload, noteId }) => {
  const url = `${ROOT}/${noteId}`;

  const { data } = await axios.get(url, payload);

  return data;
};

export const editNote = async ({ payload, noteId }) => {
  const url = `${ROOT}/${noteId}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteNotes = async ({ noteIds }) => {
  const url = `${ROOT}/delete?noteIds=${noteIds.join(',')}`;

  const { data } = await axios.delete(url);

  return data;
};

export const filterNotes = async ({ name, folderId }) => {
  const url = `${ROOT}/filter?name=${name}&folderId=${folderId}`;

  const { data } = await axios.get(url);

  return data;
};

export const activateNote = async ({ noteId }) => {
  const url = `${ROOT}/${noteId}/activate`;

  const { data } = await axios.get(url);

  return data;
};

export const deactivateNote = async ({ noteId }) => {
  const url = `${ROOT}/${noteId}/deactivate`;

  const { data } = await axios.get(url);

  return data;
};

export const fetchRecentlyDeletedNotes = async () => {
  const url = `${ROOT}/deleted`;

  const { data } = await axios.get(url);

  return data;
};

export const fetchPopularNotes = async () => {
  const url = `${ROOT}/popular?ordering=-rating&page=1`;

  const { data } = await axios.get(url);

  return data;
};

export const updateNoteRating = async ({ payload, noteId }) => {
  const url = `${ROOT}/${noteId}/rating`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const fetchLikes = async () => {
  const url = '/likes/list';

  const { data } = await axios.get(url);

  return data;
};

export const fetchUserNotes = async ({ author, name, page }) => {
  let url = `/notes/list`;

  if (author && name) {
    url += `?author=${author}&name=${name}`;
  } else if (!author && name) {
    url += `?ordering=-rating&name=${name}`;
  } else if (author && !name) {
    url += `?author=${author}`;
  }
  url += `&page=${page}`;

  const { data } = await axios.get(url);

  return data;
};
