import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import foldersReducer, { ROOT as FOLDERS_ROOT } from 'Reducers/folders';
import globalReducer, { ROOT as GLOBAL_ROOT } from 'Reducers/global';
import notesReducer, { ROOT as NOTES_ROOT } from 'Reducers/notes';
import sessionReducer, { ROOT as SESSION_ROOT } from 'Reducers/session';

const tokenPersistConfig = {
  key: 'token',
  storage,
  whitelist: ['token'],
};

const rootReducer = combineReducers({
  [SESSION_ROOT]: persistReducer(tokenPersistConfig, sessionReducer),
  [FOLDERS_ROOT]: foldersReducer,
  [GLOBAL_ROOT]: globalReducer,
  [NOTES_ROOT]: notesReducer,
});

export default rootReducer;
