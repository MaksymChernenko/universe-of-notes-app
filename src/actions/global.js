const namespace = 'GLOBAL';

export const TOGGLE_FOLDERS_LIST = `${namespace}/TOGGLE_FOLDERS_LIST`;

export const toggleFoldersList = () => ({
  type: TOGGLE_FOLDERS_LIST,
});
