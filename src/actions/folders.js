import { takeEvery } from 'redux-saga/effects';

import * as api from 'Api/folders';

import {
  createActionCreator,
  createRequestTypes,
  createSagaWorker,
  START,
  SUCCESS,
  ERROR,
} from 'Services/reduxHelpers';

const namespace = 'FOLDERS';

export const SELECT_FOLDER = `${namespace}/SELECT_FOLDER`;

export const selectFolder = ({ selectedFolder }) => ({
  type: SELECT_FOLDER,
  data: { selectedFolder },
});

export const RESET_FOLDER_DELETABLE_STATE = `${namespace}/RESET_FOLDER_DELETABLE_STATE`;

export const resetFolderDeletableState = () => ({
  type: RESET_FOLDER_DELETABLE_STATE,
});

export const SET_FOLDER_DELETABLE_STATE = `${namespace}/SET_FOLDER_DELETABLE_STATE`;

export const setFolderDeletableState = () => ({
  type: SET_FOLDER_DELETABLE_STATE,
});

export const FETCH_FOLDERS = createRequestTypes(`${namespace}/FETCH_FOLDERS`);

export const fetchFolders = createActionCreator({
  type: FETCH_FOLDERS[START],
});

const fetchFoldersWorker = createSagaWorker({
  _apiCall: api.fetchFolders,
  successActionType: FETCH_FOLDERS[SUCCESS],
  errorActionType: FETCH_FOLDERS[ERROR],
});

export const CREATE_FOLDER = createRequestTypes(`${namespace}/CREATE_FOLDER`);

export const createFolder = createActionCreator({
  type: CREATE_FOLDER[START],
  expect: ['name', 'userId'],
});

const createFolderWorker = createSagaWorker({
  _apiCall: api.createFolder,
  successActionType: CREATE_FOLDER[SUCCESS],
  errorActionType: CREATE_FOLDER[ERROR],
});

export const EDIT_FOLDER = createRequestTypes(`${namespace}/EDIT_FOLDER`);

export const editFolder = createActionCreator({
  type: EDIT_FOLDER[START],
  expect: ['folderId'],
});

const editFolderWorker = createSagaWorker({
  _apiCall: api.editFolder,
  successActionType: EDIT_FOLDER[SUCCESS],
  errorActionType: EDIT_FOLDER[ERROR],
});

export const DELETE_FOLDER = createRequestTypes(`${namespace}/DELETE_FOLDER`);

export const deleteFolder = createActionCreator({
  type: DELETE_FOLDER[START],
  expect: ['folderId'],
});

const deleteFolderWorker = createSagaWorker({
  _apiCall: api.deleteFolder,
  successActionType: DELETE_FOLDER[SUCCESS],
  errorActionType: DELETE_FOLDER[ERROR],
});

export const FILTER_FOLDERS = createRequestTypes(`${namespace}/FILTER_FOLDERS`);

export const filterFolders = createActionCreator({
  type: FILTER_FOLDERS[START],
  expect: ['name'],
});

const filterFoldersWorker = createSagaWorker({
  _apiCall: api.filterFolders,
  successActionType: FILTER_FOLDERS[SUCCESS],
  errorActionType: FILTER_FOLDERS[ERROR],
});

export const DISABLE_FOLDERS = `${namespace}/DISABLE_FOLDERS`;

export const disableFolders = () => ({
  type: DISABLE_FOLDERS,
});

export const ENABLE_FOLDERS = `${namespace}/ENABLE_FOLDERS`;

export const enableFolders = () => ({
  type: ENABLE_FOLDERS,
});

export const RESET_FOLDER_STATE = `${namespace}/RESET_FOLDER_STATE`;

export const resetFolderState = () => ({
  type: RESET_FOLDER_STATE,
});

export function* foldersWatcher() {
  yield takeEvery(FETCH_FOLDERS[START], fetchFoldersWorker);
  yield takeEvery(CREATE_FOLDER[START], createFolderWorker);
  yield takeEvery(EDIT_FOLDER[START], editFolderWorker);
  yield takeEvery(DELETE_FOLDER[START], deleteFolderWorker);
  yield takeEvery(FILTER_FOLDERS[START], filterFoldersWorker);
}
