import * as R from 'ramda';
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import * as api from 'Api/notes';

import {
  createActionCreator,
  createRequestTypes,
  createSagaWorker,
  START,
  SUCCESS,
  ERROR,
} from 'Services/reduxHelpers';

const namespace = 'NOTES';

export const SELECT_NOTE = `${namespace}/SELECT_NOTE`;

export const selectNote = ({ selectedNote }) => ({
  type: SELECT_NOTE,
  data: { selectedNote },
});

export const RESET_NOTE_DELETABLE_STATE = `${namespace}/RESET_NOTE_DELETABLE_STATE`;

export const resetNoteDeletableState = () => ({
  type: RESET_NOTE_DELETABLE_STATE,
});

export const SET_NOTE_DELETABLE_STATE = `${namespace}/SET_NOTE_DELETABLE_STATE`;

export const setNoteDeletableState = () => ({
  type: SET_NOTE_DELETABLE_STATE,
});

export const FETCH_NOTES = createRequestTypes(`${namespace}/FETCH_NOTES`);

export const fetchNotes = createActionCreator({
  type: FETCH_NOTES[START],
  expect: ['folderId'],
});

export const FETCH_RECENTLY_DELETED_NOTES = createRequestTypes(
  `${namespace}/FETCH_RECENTLY_DELETED_NOTES`,
);

export const fetchRecentlyDeletedNotes = createActionCreator({
  type: FETCH_RECENTLY_DELETED_NOTES[START],
});

export const FETCH_POPULAR_NOTES = createRequestTypes(
  `${namespace}/FETCH_POPULAR_NOTES`,
);

export const fetchPopularNotes = createActionCreator({
  type: FETCH_POPULAR_NOTES[START],
});

const fetchPopularNotesWorker = createSagaWorker({
  _apiCall: api.fetchPopularNotes,
  successActionType: FETCH_POPULAR_NOTES[SUCCESS],
  errorActionType: FETCH_POPULAR_NOTES[ERROR],
});

export const FETCH_LIKES = createRequestTypes(`${namespace}/FETCH_LIKES`);

export const fetchLikes = createActionCreator({
  type: FETCH_LIKES[START],
});

const fetchLikesWorker = createSagaWorker({
  _apiCall: api.fetchLikes,
  successActionType: FETCH_LIKES[SUCCESS],
  errorActionType: FETCH_LIKES[ERROR],
});

export const CREATE_NOTE = createRequestTypes(`${namespace}/CREATE_NOTE`);

export const createNote = createActionCreator({
  type: CREATE_NOTE[START],
  expect: ['name', 'folderId', 'userId'],
});

const createNoteWorker = createSagaWorker({
  _apiCall: api.createNote,
  successActionType: CREATE_NOTE[SUCCESS],
  errorActionType: CREATE_NOTE[ERROR],
});

export const GET_NOTE = createRequestTypes(`${namespace}/GET_NOTE`);

export const getNote = createActionCreator({
  type: GET_NOTE[START],
  expect: ['noteId'],
});

const getNoteWorker = createSagaWorker({
  _apiCall: api.getNote,
  successActionType: GET_NOTE[SUCCESS],
  errorActionType: GET_NOTE[ERROR],
});

export const EDIT_NOTE = createRequestTypes(`${namespace}/EDIT_NOTE`);

export const editNote = createActionCreator({
  type: EDIT_NOTE[START],
  expect: ['noteId'],
});

const editNoteWorker = createSagaWorker({
  _apiCall: api.editNote,
  successActionType: EDIT_NOTE[SUCCESS],
  errorActionType: EDIT_NOTE[ERROR],
});

export const DELETE_NOTES = createRequestTypes(`${namespace}/DELETE_NOTES`);

export const deleteNotes = createActionCreator({
  type: DELETE_NOTES[START],
  expect: ['noteIds'],
});

const deleteNotesWorker = createSagaWorker({
  _apiCall: api.deleteNotes,
  successActionType: DELETE_NOTES[SUCCESS],
  errorActionType: DELETE_NOTES[ERROR],
});

export const FILTER_NOTES = createRequestTypes(`${namespace}/FILTER_NOTES`);

export const filterNotes = createActionCreator({
  type: FILTER_NOTES[START],
  expect: ['name', 'folderId'],
});

const filterNotesWorker = createSagaWorker({
  _apiCall: api.filterNotes,
  successActionType: FILTER_NOTES[SUCCESS],
  errorActionType: FILTER_NOTES[ERROR],
});

export const DISABLE_NOTES = `${namespace}/DISABLE_NOTES`;

export const disableNotes = () => ({
  type: DISABLE_NOTES,
});

export const ENABLE_NOTES = `${namespace}/ENABLE_NOTES`;

export const enableNotes = () => ({
  type: ENABLE_NOTES,
});

export const RESET_NOTE_STATE = `${namespace}/RESET_NOTE_STATE`;

export const resetNoteState = () => ({
  type: RESET_NOTE_STATE,
});

export const ACTIVATE_NOTE = createRequestTypes(`${namespace}/ACTIVATE_NOTE`);

export const activateNote = createActionCreator({
  type: ACTIVATE_NOTE[START],
  expect: ['noteId'],
});

const activateNoteWorker = createSagaWorker({
  _apiCall: api.activateNote,
  successActionType: ACTIVATE_NOTE[SUCCESS],
  errorActionType: ACTIVATE_NOTE[ERROR],
});

export const DEACTIVATE_NOTE = createRequestTypes(
  `${namespace}/DEACTIVATE_NOTE`,
);

export const deactivateNote = createActionCreator({
  type: DEACTIVATE_NOTE[START],
  expect: ['noteId'],
});

export const UPDATE_NOTE_RATING = createRequestTypes(
  `${namespace}/UPDATE_NOTE_RATING`,
);

export const updateNoteRating = createActionCreator({
  type: UPDATE_NOTE_RATING[START],
  expect: ['noteId'],
});

const updateNoteRatingWorker = createSagaWorker({
  _apiCall: api.updateNoteRating,
  successActionType: UPDATE_NOTE_RATING[SUCCESS],
  errorActionType: UPDATE_NOTE_RATING[ERROR],
});

const deactivateNoteWorker = createSagaWorker({
  _apiCall: api.deactivateNote,
  successActionType: DEACTIVATE_NOTE[SUCCESS],
  errorActionType: DEACTIVATE_NOTE[ERROR],
});

function* fetchNotesWorker({ onError, onSuccess, type, ...params }) {
  const isFetchNotes = type === FETCH_NOTES[START];

  try {
    const response = yield call(
      isFetchNotes ? api.fetchNotes : api.fetchRecentlyDeletedNotes,
      params,
    );
    yield put({
      type: isFetchNotes
        ? FETCH_NOTES[SUCCESS]
        : FETCH_RECENTLY_DELETED_NOTES[SUCCESS],
      response,
    });
    if (onSuccess) {
      onSuccess(response);
    }
  } catch (error) {
    yield put({
      type: isFetchNotes
        ? FETCH_NOTES[ERROR]
        : FETCH_RECENTLY_DELETED_NOTES[ERROR],
      error,
    });
    if (onError) {
      const responseData = {
        data: R.pathOr('', ['response', 'data'])(error),
        status: R.pathOr('', ['response', 'status'])(error),
        statusText: R.pathOr('', ['response', 'statusText'])(error),
      };
      onError(responseData);
    }
  }
}

export const FETCH_USER_NOTES = createRequestTypes(
  `${namespace}/FETCH_USER_NOTES`,
);

export const fetchUserNotes = createActionCreator({
  type: FETCH_USER_NOTES[START],
  expect: ['page'],
});

const fetchUserNotesWorker = createSagaWorker({
  _apiCall: api.fetchUserNotes,
  successActionType: FETCH_USER_NOTES[SUCCESS],
  errorActionType: FETCH_USER_NOTES[ERROR],
});

export function* notesWatcher() {
  yield takeLatest(
    [FETCH_NOTES[START], FETCH_RECENTLY_DELETED_NOTES[START]],
    fetchNotesWorker,
  );
  yield takeEvery(CREATE_NOTE[START], createNoteWorker);
  yield takeEvery(GET_NOTE[START], getNoteWorker);
  yield takeEvery(EDIT_NOTE[START], editNoteWorker);
  yield takeEvery(DELETE_NOTES[START], deleteNotesWorker);
  yield takeLatest(FILTER_NOTES[START], filterNotesWorker);
  yield takeEvery(ACTIVATE_NOTE[START], activateNoteWorker);
  yield takeEvery(DEACTIVATE_NOTE[START], deactivateNoteWorker);
  yield takeLatest(FETCH_POPULAR_NOTES[START], fetchPopularNotesWorker);
  yield takeEvery(FETCH_LIKES[START], fetchLikesWorker);
  yield takeEvery(UPDATE_NOTE_RATING[START], updateNoteRatingWorker);
  yield takeLatest(FETCH_USER_NOTES[START], fetchUserNotesWorker);
}
