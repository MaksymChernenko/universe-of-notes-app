import * as R from 'ramda';

import * as actions from 'Actions/session';

import { ERROR, SUCCESS, START, FETCHING_STATE } from 'Services/reduxHelpers';

export const ROOT = 'session';

const initialState = {
  isAuthenticated: false,
  isRefreshed: false,
  user: null,
  token: '',
  popularUsers: {
    data: [],
    state: null,
  },
};

const sessionReducer = (state = initialState, { type, response }) => {
  let data = null;

  switch (type) {
    case actions.REGISTER[SUCCESS]:
    case actions.LOGIN[SUCCESS]:
      data = response.data;
      return R.mergeDeepRight(state, {
        isAuthenticated: true,
        isRefreshed: true,
        user: {
          id: data.id,
          username: data.username,
          email: data.email,
        },
        token: data.token,
      });
    case actions.REFRESH_USER[SUCCESS]:
      data = response.data;
      return R.mergeDeepRight(state, {
        isAuthenticated: true,
        isRefreshed: true,
        user: {
          id: data.id,
          username: data.username,
          email: data.email,
        },
      });
    case actions.REGISTER[ERROR]:
    case actions.LOGIN[ERROR]:
    case actions.REFRESH_USER[ERROR]:
    case actions.LOGOUT[SUCCESS]:
      return R.mergeDeepRight(state, {
        isAuthenticated: false,
        isRefreshed: true,
        user: null,
        token: '',
        popularUsers: {
          data: [],
          state: null,
        },
      });
    case actions.UPDATE_USER[SUCCESS]:
      data = response.data;
      return R.mergeDeepRight(state, {
        user: { ...state.user, ...data },
      });
    case actions.FETCH_POPULAR_USERS[START]:
      return R.mergeDeepRight(state, {
        popularUsers: {
          data: R.pathOr([], [ROOT, 'data'], state),
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_POPULAR_USERS[SUCCESS]:
      return R.mergeDeepRight(state, {
        popularUsers: {
          data: R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_POPULAR_USERS[ERROR]:
      return R.mergeDeepRight(state, {
        popularUsers: {
          data: [],
          state: FETCHING_STATE.ERROR,
        },
      });
    default:
      return state;
  }
};

export default sessionReducer;
