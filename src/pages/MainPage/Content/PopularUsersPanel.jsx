import PropTypes from 'prop-types';

import { UserOutlined } from '@ant-design/icons';

import React, { Fragment } from 'react';

import {
  AuthorLink,
  PopularUsersCard,
  PopularUsersWrapper,
  PopularUsersCardContentBlock,
} from '../styledComponents';

const PopularUsersPanel = ({ title, loading, authors }) => (
  <PopularUsersWrapper>
    <PopularUsersCard loading={loading} title={title}>
      <PopularUsersCardContentBlock>
        {authors.map(item => (
          <Fragment key={item.id}>
            <AuthorLink to={`/app/user-details/${item.username}`}>
              {item.username}
            </AuthorLink>
            <UserOutlined />
          </Fragment>
        ))}
      </PopularUsersCardContentBlock>
    </PopularUsersCard>
  </PopularUsersWrapper>
);

PopularUsersPanel.propTypes = {
  loading: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  authors: PropTypes.array.isRequired,
};

export default PopularUsersPanel;
