import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchPopularUsers } from 'Actions/session';
import { getPopularUsers } from 'Selectors/session';
import {
  fetchPopularNotes,
  updateNoteRating,
  fetchLikes,
  resetNoteState,
} from 'Actions/notes';
import { getNotes, getLikes, getUserNotes } from 'Selectors/notes';
import { FETCHING_STATE } from 'Services/reduxHelpers';

import { getNotePreviewText, isNoteEmpty } from 'Utils';

import {
  Wrapper,
  MainLeftBlock,
  MainRightBlock,
  CardsWrapper,
  MainDivider,
} from './styledComponents';
import NoteCard from './Content/NoteCard';
import PopularUsersPanel from './Content/PopularUsersPanel';

const MainPage = () => {
  const dispatch = useDispatch();

  const [currentNotes, setCurrentNotes] = useState([]);
  const [loadingPage, setLoadingPage] = useState(true);

  const popularUsers = useSelector(getPopularUsers);
  const notes = useSelector(getNotes);
  const likes = useSelector(getLikes);
  const filterNotes = useSelector(getUserNotes);

  const isPopularNotesFetching = notes.state === FETCHING_STATE.FETCHING;
  const isFilterNotesFetching = filterNotes.state === FETCHING_STATE.FETCHING;
  const isFilterNotesLoaded = filterNotes.state === FETCHING_STATE.LOADED;

  useEffect(() => {
    return () => {
      dispatch(resetNoteState());
    };
  }, []);

  useEffect(() => {
    dispatch(fetchPopularNotes());
    dispatch(fetchPopularUsers());
    dispatch(fetchLikes());
  }, []);

  useEffect(() => {
    if (
      notes.state === FETCHING_STATE.LOADED &&
      popularUsers.state === FETCHING_STATE.LOADED &&
      likes.state === FETCHING_STATE.LOADED
    ) {
      setLoadingPage(false);
      setCurrentNotes({
        state: notes.state,
        data: notes.data.filter(item => !isNoteEmpty(item)),
      });
    }
  }, [notes, popularUsers, likes]);

  useEffect(() => {
    if (isFilterNotesFetching) {
      setLoadingPage(true);
    } else if (isFilterNotesLoaded) {
      setLoadingPage(false);
      setCurrentNotes({
        state: filterNotes.state,
        data: filterNotes.data.filter(item => !isNoteEmpty(item)),
      });
    }
  }, [filterNotes]);

  useEffect(() => {
    if (isPopularNotesFetching) {
      setLoadingPage(true);
      setCurrentNotes([]);
    }
  }, [notes]);

  const toggleLikeButton = (noteId, isLike) => {
    dispatch(
      updateNoteRating({
        noteId,
        payload: {
          rating: isLike ? 1 : 0,
        },
      }),
    );
  };

  return (
    <Wrapper>
      <MainLeftBlock>
        <CardsWrapper>
          {loadingPage &&
            Array.from({ length: 6 }, (v, k) => k + 1).map(item => (
              <NoteCard
                key={`${item}_loading`}
                title="Some"
                id={item}
                loading
                previewContent="Some"
                author="Some"
                favoriteNum={1}
                clickOnLike={() => null}
                isLikedBefore={false}
                setNote={() => null}
              />
            ))}
          {currentNotes.state === FETCHING_STATE.LOADED && !loadingPage && (
            <>
              {currentNotes.data.map(item => (
                <NoteCard
                  key={item.id}
                  id={item.id}
                  loading={false}
                  title={item.name}
                  previewContent={getNotePreviewText(item)}
                  author={item.author}
                  favoriteNum={item.rating}
                  clickOnLike={toggleLikeButton}
                  isLikedBefore={
                    !!likes.data.find(like => like.noteId === item.id)
                  }
                />
              ))}
            </>
          )}
        </CardsWrapper>
      </MainLeftBlock>
      <MainRightBlock>
        <MainDivider />
        <PopularUsersPanel
          loading={loadingPage}
          title="Popular users"
          authors={popularUsers.data}
        />
      </MainRightBlock>
    </Wrapper>
  );
};

export default MainPage;
