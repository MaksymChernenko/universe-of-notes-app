import 'draft-js/dist/Draft.css';

import PropTypes from 'prop-types';

import { LockOutlined } from '@ant-design/icons';

import {
  convertFromRaw,
  convertToRaw,
  EditorState,
  getDefaultKeyBinding,
  KeyBindingUtil,
  RichUtils,
} from 'draft-js';
import Editor from 'draft-js-plugins-editor';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { editNote } from 'Actions/notes';
import { getSelectedFolder } from 'Selectors/folders';
import { getNotes, getSelectedNote } from 'Selectors/notes';

import { FETCHING_STATE } from 'Services/reduxHelpers';
import {
  getNoteDate,
  NOTIFICATION_TYPES,
  openNotificationWithIcon,
} from 'Utils';
import plugins, {
  addVideo,
  AlignmentTool,
  CharCounter,
  EmojiSelect,
  EmojiSuggestions,
  LineCounter,
  RedoButton,
  UndoButton,
  WordCounter,
} from './plugins';
import { getBlockStyle, getColorsStyleMap, pickerColors } from './utils';

import RecentlyDeletedOverlay from './RecentlyDeletedOverlay';
import Toolbar from './Toolbar';

const NoteEditor = ({ isFoldersListCollapsed }) => {
  const dispatch = useDispatch();
  const editor = useRef(null);

  const [editorState, setEditorState] = useState(null);
  const [readOnly, setReadOnly] = useState(false);
  const [isSaving, setIsSaving] = useState(false);

  const notes = useSelector(getNotes);
  const selectedFolder = useSelector(getSelectedFolder);
  const selectedNote = useSelector(getSelectedNote);

  useEffect(() => {
    if (!selectedNote.id) return;

    const { content } = selectedNote;

    if (content) {
      setEditorState(
        EditorState.createWithContent(convertFromRaw(JSON.parse(content))),
      );
    } else {
      setEditorState(EditorState.createEmpty());
    }
  }, [selectedNote.id]);

  useEffect(() => {
    if (selectedNote.locked) {
      setReadOnly(true);
    } else {
      setReadOnly(false);
    }
  }, [selectedNote.locked]);

  const toggleReadOnlyState = () => setReadOnly(prev => !prev);

  const focusEditor = () => {
    editor.current.focus();
  };

  const handleKeyCommand = (command, prevState) => {
    const newState = RichUtils.handleKeyCommand(prevState, command);

    if (newState) {
      setEditorState(newState);
      return 'handled';
    }

    return 'not-handled';
  };

  const handleTab = e => {
    if (e.keyCode !== 9) return getDefaultKeyBinding(e);

    const newEditorState = RichUtils.onTab(e, editorState, 4);
    if (newEditorState !== editorState) {
      setEditorState(newEditorState);
    }
  };

  const onChange = newState => setEditorState(newState);

  const saveNote = () => {
    const content = JSON.stringify(
      convertToRaw(editorState.getCurrentContent()),
    );
    setIsSaving(true);
    dispatch(
      editNote({
        noteId: selectedNote.id,
        payload: {
          content,
        },
        onSuccess: ({ data }) => {
          selectedNote.setNotesMenuItems(prev =>
            prev.map(item =>
              item.id === data.id
                ? {
                    ...item,
                    content: data.content,
                    updateDt: data.updateDt,
                  }
                : item,
            ),
          );

          setIsSaving(false);
        },
        onError: ({ data }) => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: data?.data ? data.data : 'Edit note error',
          });
          setIsSaving(false);
        },
      }),
    );
  };

  const saveCommandBindingFn = e => {
    const { hasCommandModifier } = KeyBindingUtil;
    if (e.keyCode === 83 && hasCommandModifier(e)) {
      e.preventDefault();
      saveNote();
      return;
    }
    return getDefaultKeyBinding(e);
  };

  return (
    <NoteEditor.Wrapper isFoldersListCollapsed={isFoldersListCollapsed}>
      {selectedNote.id && editorState && (
        <>
          <NoteEditor.Date>{getNoteDate(selectedNote)}</NoteEditor.Date>
          <Toolbar
            addVideo={addVideo}
            editorState={editorState}
            setEditorState={setEditorState}
            readOnly={readOnly}
            toggleReadOnlyState={toggleReadOnlyState}
            saveNote={saveNote}
            redoButton={RedoButton}
            undoButton={UndoButton}
            emojiSuggestions={EmojiSuggestions}
            emojiSelect={EmojiSelect}
            isSaving={isSaving}
            locked={selectedNote.locked}
          />
          <NoteEditor.EditorWrapper readOnly={readOnly} onClick={focusEditor}>
            <div>
              <Editor
                ref={editor}
                blockStyleFn={getBlockStyle}
                editorState={editorState}
                onChange={onChange}
                onTab={handleTab}
                handleKeyCommand={handleKeyCommand}
                keyBindingFn={saveCommandBindingFn}
                plugins={plugins}
                readOnly={readOnly}
                customStyleMap={getColorsStyleMap(pickerColors)}
              />
              {!readOnly && <AlignmentTool />}
              <NoteEditor.CounterWrapper>
                <NoteEditor.Counter>
                  <CharCounter limit={1000} /> characters
                </NoteEditor.Counter>
                <NoteEditor.Counter>
                  <WordCounter /> words
                </NoteEditor.Counter>
                <NoteEditor.Counter>
                  <LineCounter /> lines
                </NoteEditor.Counter>
              </NoteEditor.CounterWrapper>
              {selectedNote.locked && (
                <NoteEditor.Locked>
                  <LockOutlined style={{ fontSize: 20 }} />
                </NoteEditor.Locked>
              )}
            </div>
          </NoteEditor.EditorWrapper>
          {selectedFolder.name === 'Recently Deleted' &&
            notes.state === FETCHING_STATE.LOADED && <RecentlyDeletedOverlay />}
        </>
      )}
    </NoteEditor.Wrapper>
  );
};

NoteEditor.propTypes = {
  isFoldersListCollapsed: PropTypes.bool.isRequired,
};

NoteEditor.Date = styled.p`
  text-align: center;
  color: var(--hurricane);
  margin-bottom: 14px;
`;

NoteEditor.Counter = styled.div`
  margin-right: 20px;
  font-weight: 300;

  & > span {
    font-weight: 400;
  }
`;

NoteEditor.CounterWrapper = styled.div`
  position: absolute;
  display: flex;
  bottom: 2px;
  left: 30px;
`;

NoteEditor.EditorWrapper = styled.div`
  height: calc(100vh - 214px);
  overflow-y: auto;
  background-color: ${({ readOnly }) =>
    readOnly ? 'inherit' : 'var(--alabaster)'};
  padding: 10px;

  & .draftJsFocusPlugin__focused__3Mksn {
    box-shadow: ${({ readOnly }) => (readOnly ? 'none' : '0 0 0 3px #ACCEF7')};
  }

  & .draftJsFocusPlugin__unfocused__1Wvrs:hover {
    box-shadow: ${({ readOnly }) => (readOnly ? 'none' : '0 0 0 3px #D2E3F7')};
  }
`;

NoteEditor.Locked = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  color: var(--hurricane);
`;

NoteEditor.Wrapper = styled.div`
  position: relative;
  min-width: 224px;
  background: linear-gradient(to bottom, var(--white), transparent 500%);
  height: calc(100vh - 86px);
  padding: 20px;
  width: ${({ isFoldersListCollapsed }) =>
    isFoldersListCollapsed ? '85%' : '70%'};
`;

export default NoteEditor;
