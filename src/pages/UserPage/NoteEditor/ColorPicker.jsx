import PropTypes from 'prop-types';

import { Button, Tooltip } from 'antd';
import { FontColorsOutlined } from '@ant-design/icons';

import React, { useEffect, useRef, useState } from 'react';
import { GithubPicker } from 'react-color';
import styled from 'styled-components';

import { applyBlur } from 'Utils';
import { pickerColors } from './utils';

const ColorPicker = ({ onStyleClick }) => {
  const wrapperRef = useRef(null);

  const [color, setColor] = useState('#102947');
  const [open, setOpen] = useState(false);

  const handleOutsideClick = e => {
    if (!wrapperRef.current?.contains(e.target)) {
      setOpen(false);
    }
  };

  useEffect(() => {
    if (open) {
      document.addEventListener('click', handleOutsideClick);
    } else {
      document.removeEventListener('click', handleOutsideClick);
    }
  }, [open]);

  const clickButtonHandler = e => {
    applyBlur(e);
    setOpen(prev => !prev);
  };

  const changeColorHandler = ({ hex }) => {
    setColor(hex);
    onStyleClick(null, hex);
  };

  return (
    <Tooltip mouseEnterDelay={1} placement="topRight" title="set color">
      <ColorPicker.Wrapper ref={wrapperRef}>
        <ColorPicker.Button
          onClick={clickButtonHandler}
          size="small"
          icon={<FontColorsOutlined />}
        />
        {open && (
          <ColorPicker.Container>
            <GithubPicker
              width={112}
              color={color}
              colors={pickerColors}
              onChangeComplete={changeColorHandler}
            />
          </ColorPicker.Container>
        )}
      </ColorPicker.Wrapper>
    </Tooltip>
  );
};

ColorPicker.propTypes = {
  onStyleClick: PropTypes.func.isRequired,
};

ColorPicker.Button = styled(Button)`
  color: var(--blue-zodiac);
  border: 1px solid var(--blue-zodiac);
  margin-right: 10px;
  background-color: inherit;
`;

ColorPicker.Container = styled.div`
  position: absolute;
  top: 140%;
  left: -6px;
  z-index: 10;
`;

ColorPicker.Wrapper = styled.div`
  position: relative;
`;

export default ColorPicker;
