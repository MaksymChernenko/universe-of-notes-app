import PropTypes from 'prop-types';

import { Select } from 'antd';

import React, { useRef } from 'react';
import styled from 'styled-components';

import { BLOCK_TYPE_HEADINGS, getBlockTypeHeadingsValue } from './utils';

const HeadingsSelect = ({ editorState, toggleBlockType }) => {
  const selectRef = useRef(null);

  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <HeadingsSelect.Wrapper>
      <Select
        size="small"
        ref={selectRef}
        dropdownClassName="heading-select"
        value={getBlockTypeHeadingsValue(blockType)}
        onChange={value => {
          toggleBlockType(value);
          if (selectRef) {
            selectRef.current.blur();
          }
        }}
      >
        <Select.Option value="">Regular</Select.Option>
        {BLOCK_TYPE_HEADINGS.map(heading => (
          <Select.Option key={heading.label} value={heading.style}>
            {heading.label}
          </Select.Option>
        ))}
      </Select>
    </HeadingsSelect.Wrapper>
  );
};

HeadingsSelect.Wrapper = styled.div`
  margin-right: 10px;

  & .ant-select {
    width: 120px;
  }

  & .ant-select:hover .ant-select-selector,
  & .ant-select-focused .ant-select-selector {
    border-color: var(--black) !important;
    box-shadow: none;
  }
`;

HeadingsSelect.propTypes = {
  editorState: PropTypes.object.isRequired,
  toggleBlockType: PropTypes.func.isRequired,
};

export default HeadingsSelect;
