import 'draft-js-alignment-plugin/lib/plugin.css';

import createAlignmentPlugin from 'draft-js-alignment-plugin';

export const alignmentPlugin = createAlignmentPlugin();

export const { AlignmentTool } = alignmentPlugin;

export default alignmentPlugin;
