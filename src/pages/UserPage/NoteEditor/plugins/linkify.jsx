import 'draft-js-linkify-plugin/lib/plugin.css';

import createLinkifyPlugin from 'draft-js-linkify-plugin';

const linkifyPlugin = createLinkifyPlugin({
  target: '_blank',
});

export default linkifyPlugin;
