import 'Styles/undoPluginStyles.css';

import { Button } from 'antd';
import { RedoOutlined, UndoOutlined } from '@ant-design/icons';

import createUndoPlugin from 'draft-js-undo-plugin';
import React from 'react';
import styled from 'styled-components';

const StateButton = styled(Button)`
  padding: 0 4px;
  color: var(--blue-zodiac);
  border: 1px solid var(--blue-zodiac);
  margin-right: 10px;
  background-color: inherit;
`;

const theme = {
  undo: 'undoRedoButton',
  redo: 'undoRedoButton',
};

const undoPlugin = createUndoPlugin({
  redoContent: <StateButton icon={<RedoOutlined />} size="small" />,
  undoContent: <StateButton icon={<UndoOutlined />} size="small" />,
  theme,
});

export const { UndoButton, RedoButton } = undoPlugin;

export default undoPlugin;
