import createResizablePlugin from 'draft-js-resizeable-plugin';

const resizablePlugin = createResizablePlugin();

export default resizablePlugin;
