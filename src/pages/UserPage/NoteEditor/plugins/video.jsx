import { composeDecorators } from 'draft-js-plugins-editor';
import createVideoPlugin from 'draft-js-video-plugin';

import alignmentPlugin from './alignment';
import focusPlugin from './focus';
import resizablePlugin from './resizable';

const decorator = composeDecorators(
  alignmentPlugin.decorator,
  focusPlugin.decorator,
  resizablePlugin.decorator,
);

const videoPlugin = createVideoPlugin({ decorator });

export const { addVideo } = videoPlugin;

export default videoPlugin;
