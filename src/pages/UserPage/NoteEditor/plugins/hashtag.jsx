import 'draft-js-hashtag-plugin/lib/plugin.css';

import createHashtagPlugin from 'draft-js-hashtag-plugin';

const hashtagPlugin = createHashtagPlugin();

export default hashtagPlugin;
