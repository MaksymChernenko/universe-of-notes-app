import 'draft-js-emoji-plugin/lib/plugin.css';

import { Button } from 'antd';
import { SmileOutlined } from '@ant-design/icons';

import createEmojiPlugin from 'draft-js-emoji-plugin';
import React from 'react';
import styled from 'styled-components';

const StateButton = styled(Button)`
  padding: 0 4px;
  color: var(--blue-zodiac);
  border: 1px solid var(--blue-zodiac);
  margin-right: 10px;
  background-color: inherit;
`;

const emojiPlugin = createEmojiPlugin({
  selectButtonContent: <StateButton icon={<SmileOutlined />} size="small" />,
});

export const { EmojiSelect, EmojiSuggestions } = emojiPlugin;

export default emojiPlugin;
