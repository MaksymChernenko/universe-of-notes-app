import 'draft-js-focus-plugin/lib/plugin.css';

import createFocusPlugin from 'draft-js-focus-plugin';

const focusPlugin = createFocusPlugin();

export default focusPlugin;
