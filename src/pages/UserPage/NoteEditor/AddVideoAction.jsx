import PropTypes from 'prop-types';

import { Button, Input, Popover, Tooltip } from 'antd';
import { PlusOutlined, VideoCameraAddOutlined } from '@ant-design/icons';

import React, { useState } from 'react';
import styled from 'styled-components';

import { applyBlur } from 'Utils';

const AddVideoAction = ({ addVideo, editorState, setEditorState }) => {
  const [visible, setVisible] = useState(false);
  const [link, setLink] = useState('');

  const handleAddVideo = e => {
    e.preventDefault();
    setEditorState(
      addVideo(editorState, {
        src: link,
      }),
    );
    setLink('');
    setVisible(false);
  };

  const content = (
    <AddVideoAction.Form onSubmit={handleAddVideo}>
      <Input
        style={{ width: 180, marginRight: 20 }}
        size="small"
        allowClear
        required
        autoComplete="off"
        value={link}
        onChange={({ target: { value } }) => setLink(value)}
        placeholder="Paste link..."
      />
      <AddVideoAction.ActionButton
        htmlType="submit"
        shape="circle"
        size="small"
        style={{ margin: 0 }}
        icon={<PlusOutlined />}
        onClick={e => applyBlur(e)}
      />
    </AddVideoAction.Form>
  );

  return (
    <Popover
      visible={visible}
      placement="bottom"
      content={content}
      trigger="click"
      onVisibleChange={visibleState => setVisible(visibleState)}
    >
      <Tooltip mouseEnterDelay={1} placement="topRight" title="add video">
        <AddVideoAction.ActionButton
          size="small"
          icon={<VideoCameraAddOutlined />}
          onClick={e => {
            setVisible(true);
            applyBlur(e);
          }}
        />
      </Tooltip>
    </Popover>
  );
};

AddVideoAction.propTypes = {
  addVideo: PropTypes.func.isRequired,
  editorState: PropTypes.object.isRequired,
  setEditorState: PropTypes.func.isRequired,
};

AddVideoAction.ActionButton = styled(Button)`
  color: var(--blue-zodiac);
  border: 1px solid var(--blue-zodiac);
  margin-right: 10px;
  background-color: inherit;
`;

AddVideoAction.Form = styled.form`
  display: flex;

  & .ant-divider-with-text::after,
  .ant-divider-with-text::before {
    border-top: 1px solid var(--blue-zodiac);
  }

  & .ant-input-affix-wrapper:hover {
    border-color: var(--blue-zodiac);
  }

  & .ant-input-affix-wrapper-focused {
    border-color: var(--blue-zodiac);
    box-shadow: 0 0 0 2px rgba(16, 41, 71, 0.2);
  }

  & .ant-btn:hover,
  & .ant-btn:focus {
    background-color: var(--merino);
    color: var(--black);
    border: 1px solid var(--black);
  }
`;

export default AddVideoAction;
