import logo from 'Assets/logo.png';

import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { routePaths } from 'Routes';
import { SIGN_FORMS } from 'Utils';

import RegisterForm from './RegisterForm';
import SignInForm from './SignInForm';

const SignPage = () => {
  const history = useHistory();

  const [form, setForm] = useState(SIGN_FORMS.SIGN_IN_FORM);

  const setRegisterForm = () => setForm(SIGN_FORMS.REGISTER_FORM);

  const setSignInForm = () => setForm(SIGN_FORMS.SIGN_IN_FORM);

  const redirectToApp = () => history.replace(routePaths.main);

  return (
    <SignPage.Wrapper>
      <div>
        <SignPage.Title>Universe of Notes</SignPage.Title>
        <SignPage.Grouper>
          <div>
            <SignPage.Logo src={logo} alt="logo" />
            <SignPage.Copyright>
              © 2020 Max Team. All rights reserved.
            </SignPage.Copyright>
          </div>
          <SignPage.Divider />
          {form === SIGN_FORMS.SIGN_IN_FORM && (
            <SignInForm
              setRegisterForm={setRegisterForm}
              redirectToApp={redirectToApp}
            />
          )}
          {form === SIGN_FORMS.REGISTER_FORM && (
            <RegisterForm
              setSignInForm={setSignInForm}
              redirectToApp={redirectToApp}
            />
          )}
        </SignPage.Grouper>
      </div>
    </SignPage.Wrapper>
  );
};

SignPage.Copyright = styled.p`
  position: absolute;
  bottom: 20px;
  left: 50%;
  transform: translate(-50%, -50%);

  @media (min-width: 768px) {
    position: unset;
    text-align: center;
    transform: none;
  }
`;

SignPage.Divider = styled.div`
  display: none;

  @media (min-width: 768px) {
    display: block;
    width: 4px;
    background: linear-gradient(
      to bottom,
      var(--soft-amber),
      var(--blue-zodiac) 280%
    );
    margin: 0 30px;
  }
`;

SignPage.Grouper = styled.div`
  display: flex;
`;

SignPage.Logo = styled.img`
  display: none;

  @media (min-width: 768px) {
    display: block;
    width: 300px;
    margin-bottom: 20px;
  }
`;

SignPage.Title = styled.h3`
  margin-bottom: 50px;
  text-align: center;
  font-size: 24px;

  @media (min-width: 768px) {
    display: none;
  }
`;

SignPage.Wrapper = styled.div`
  background: linear-gradient(to top, var(--white), transparent 50%);
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;

  @media (min-width: 768px) {
    padding: 0;
  }
`;

export default SignPage;
