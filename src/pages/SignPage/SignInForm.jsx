import PropTypes from 'prop-types';

import { Divider, Input } from 'antd';
import { KeyOutlined, UserOutlined } from '@ant-design/icons';

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { login } from 'Actions/session';

import { NOTIFICATION_TYPES, openNotificationWithIcon } from 'Utils';

import { Form, SubmitButton, ToggleFormButton } from './styledComponents';

const SignInForm = ({ redirectToApp, setRegisterForm }) => {
  const dispatch = useDispatch();

  const [userData, setUserData] = useState({
    username: '',
    password: '',
  });

  const changeHandler = ({ target: { name, value } }) => {
    setUserData(prevData => ({
      ...prevData,
      [name]: value,
    }));
  };

  const submitHandler = e => {
    e.preventDefault();
    dispatch(
      login({
        payload: userData,
        onSuccess: () => redirectToApp(),
        onError: error => {
          const { data } = error;
          Object.values(data).forEach(value => {
            openNotificationWithIcon({
              type: NOTIFICATION_TYPES.ERROR,
              message: 'Ooops!',
              description: value[0],
            });
          });
        },
      }),
    );
  };

  return (
    <Form onSubmit={submitHandler}>
      <Divider style={{ margin: '0 0 20px' }} orientation="left">
        Sign In
      </Divider>
      <Input
        autoFocus
        required
        autoComplete="off"
        name="username"
        value={userData.username}
        onChange={changeHandler}
        style={{ marginBottom: 14 }}
        allowClear
        placeholder="Enter your username or email"
        prefix={<UserOutlined />}
      />
      <Input.Password
        type="password"
        required
        autoComplete="off"
        name="password"
        value={userData.password}
        onChange={changeHandler}
        style={{ marginBottom: 14 }}
        placeholder="Enter your password"
        prefix={<KeyOutlined />}
      />
      <SubmitButton htmlType="submit">Submit</SubmitButton>
      <div>
        <span>Don&prime;t have an account yet?</span>
        <ToggleFormButton onClick={setRegisterForm} type="link">
          Register
        </ToggleFormButton>
      </div>
    </Form>
  );
};

SignInForm.propTypes = {
  redirectToApp: PropTypes.func.isRequired,
  setRegisterForm: PropTypes.func.isRequired,
};

export default SignInForm;
