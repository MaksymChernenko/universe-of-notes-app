import { Breadcrumb, Tree } from 'antd';

import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import styled from 'styled-components';

import { routePaths } from 'Routes';
import { DOCS_PAGES, getContent, getTitleByKey, treeData } from './utils';

const { DirectoryTree } = Tree;

const DocsPage = () => {
  const history = useHistory();
  const params = useParams();

  const [selectedKey, setSelectedKey] = useState();
  const [header, setHeader] = useState('');

  useEffect(() => {
    const title = getTitleByKey(params.page);
    if (!title) {
      history.push(`/docs/${DOCS_PAGES.USER_PAGE}`);
    }
    setHeader(title);
    setSelectedKey(params.page);
  }, [params]);

  const redirectToApp = () => history.push(routePaths.main);

  const selectHandler = keys => {
    const key = keys[0];
    history.push(`/docs/${key}`);
  };

  return (
    <>
      <DocsPage.Breadcrumb>
        <DocsPage.BreadcrumbItem link="true" onClick={redirectToApp}>
          App
        </DocsPage.BreadcrumbItem>
        <DocsPage.BreadcrumbItem style={{ color: 'var(--hurricane)' }}>
          Documentation
        </DocsPage.BreadcrumbItem>
      </DocsPage.Breadcrumb>
      <DocsPage.Wrapper>
        <DocsPage.DirectoryTree
          selectedKeys={[selectedKey]}
          defaultExpandAll
          onSelect={selectHandler}
          treeData={treeData}
        />
        <DocsPage.Container>
          <DocsPage.Scroll>
            <DocsPage.Title>{header}</DocsPage.Title>
            {getContent({ key: selectedKey })}
          </DocsPage.Scroll>
        </DocsPage.Container>
      </DocsPage.Wrapper>
    </>
  );
};

DocsPage.Breadcrumb = styled(Breadcrumb)`
  padding: 10px 20px;
  color: var(--blue-zodiac);
  border-bottom: 1px solid var(--blue-zodiac);
`;

DocsPage.BreadcrumbItem = styled(Breadcrumb.Item)`
  &:hover {
    cursor: ${({ link }) => (link ? `pointer` : 'default')};
    border-bottom: ${({ link }) =>
      link ? `1px solid var(--blue-zodiac)` : 'none'};
  }
`;

DocsPage.Container = styled.div`
  width: calc(100% - 250px);
  border: 1px solid var(--swiss-coffee);
  padding: 20px;
  box-shadow: inset 0 0 1px 1px var(--white);
`;

DocsPage.DirectoryTree = styled(DirectoryTree)`
  background-color: inherit;
  width: 250px;
  margin-right: 60px;

  & .ant-tree-node-content-wrapper.ant-tree-node-selected {
    background-color: var(--blue-zodiac) !important;
  }

  & .ant-tree-treenode-selected::before {
    background-color: var(--blue-zodiac) !important;
  }
`;

DocsPage.Scroll = styled.div`
  height: 100%;
  padding: 0 10px;
  overflow-y: scroll;
`;

DocsPage.Title = styled.h3`
  margin-bottom: 20px;
  color: var(--blue-zodiac);
  text-indent: 20px;
`;

DocsPage.Wrapper = styled.div`
  display: flex;
  padding: 20px;
  overflow-y: auto;
  height: calc(100vh - 43px);
`;

export default DocsPage;
