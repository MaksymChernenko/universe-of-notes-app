import React from 'react';
import styled from 'styled-components';

import { DOCS_PAGES } from 'Pages/DocsPage/utils';

import { LazyImage } from 'Components';

import createNoteButtonImg from 'Assets/docs/create-note-button.png';
import createNoteEditModeImg from 'Assets/docs/create-note-edit-mode.png';
import createNoteSavedImg from 'Assets/docs/create-note-saved.png';

import {
  ContentLink,
  ImageContainer,
  LiContent,
  Paragraph,
} from './styledComponents';

const UserPageNotesListCreate = () => (
  <>
    <Paragraph>There are two ways to delete note:</Paragraph>
    <UserPageNotesListCreate.UL>
      <li>
        <UserPageNotesListCreate.OL>
          <li>
            <LiContent>
              Click the right mouse button on the notes list to display a
              context menu.
            </LiContent>
            <ImageContainer width={200}>
              <LazyImage src={createNoteButtonImg} alt="Create Note Button" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>Enter the unique note name in folder context.</LiContent>
            <ImageContainer width={200}>
              <LazyImage src={createNoteEditModeImg} alt="Edit Note Mode" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>Press Enter or change focus to save the note.</LiContent>
            <ImageContainer width={200}>
              <LazyImage src={createNoteSavedImg} alt="Note Saved" />
            </ImageContainer>
          </li>
        </UserPageNotesListCreate.OL>
      </li>
      <li>
        <LiContent>
          Check{' '}
          <ContentLink to={`/docs/${DOCS_PAGES.USER_PAGE_ACTIONS}`}>
            User Actions
          </ContentLink>
          .
        </LiContent>
      </li>
    </UserPageNotesListCreate.UL>
  </>
);

UserPageNotesListCreate.OL = styled.ol`
  padding-left: 30px;
`;

UserPageNotesListCreate.UL = styled.ul`
  padding-left: 70px;
  list-style: disc;
`;

export default UserPageNotesListCreate;
