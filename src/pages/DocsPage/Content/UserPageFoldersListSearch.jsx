import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import searchFolders from 'Assets/docs/folders-search.png';
import searchedFolders from 'Assets/docs/folders-searched.png';

import { ImageContainer, LiContent, Paragraph } from './styledComponents';

const UserPageFoldersListSearch = () => (
  <>
    <Paragraph>To search the folders follow these steps:</Paragraph>
    <UserPageFoldersListSearch.OL>
      <li>
        <LiContent>Enter text for search.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={searchFolders} alt="Search Folders" />
        </ImageContainer>
      </li>
      <li>
        <LiContent>Press enter key or &#xab;search&#xbb; icon.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={searchedFolders} alt="Searched Folders" />
        </ImageContainer>
      </li>
    </UserPageFoldersListSearch.OL>
  </>
);

UserPageFoldersListSearch.OL = styled.ol`
  padding-left: 70px;
`;

export default UserPageFoldersListSearch;
