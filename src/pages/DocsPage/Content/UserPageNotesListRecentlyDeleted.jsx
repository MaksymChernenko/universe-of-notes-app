import React from 'react';

import { DOCS_PAGES } from 'Pages/DocsPage/utils';

import { LazyImage } from 'Components';

import recentlyDeletedFolderImg from 'Assets/docs/recently-deleted-folder.png';
import recentlyDeletedNotesImg from 'Assets/docs/recently-deleted-notes.png';
import recentlyDeletedActionsImg from 'Assets/docs/recently-deleted-actions.png';

import { ContentLink, ImageContainer, Paragraph } from './styledComponents';

const UserPageNotesListRecentlyDeleted = () => (
  <>
    <Paragraph>
      To go to recently deleted notes press &#xab;Recently Deleted&#xbb; folder
      on the folders list.
    </Paragraph>
    <ImageContainer width={200}>
      <LazyImage src={recentlyDeletedFolderImg} alt="Recently Deleted Folder" />
    </ImageContainer>
    <Paragraph>
      Then all recently deleted notes in the user context will be available. It
      should be noted that context menu & notes search do not work.
    </Paragraph>
    <ImageContainer width={200}>
      <LazyImage src={recentlyDeletedNotesImg} alt="Recently Deleted Notes" />
    </ImageContainer>
    <Paragraph>
      Any note in the folder can be restored and deleted permanently. To do this
      click on the appropriate button on the editor overlay. It also indicates
      in which folder the note will be restored.
    </Paragraph>
    <ImageContainer width={300}>
      <LazyImage
        src={recentlyDeletedActionsImg}
        alt="Recently Deleted Actions"
      />
    </ImageContainer>
    <Paragraph>
      It is possible to permanently delete all notes at once. For this check{' '}
      <ContentLink to={`/docs/${DOCS_PAGES.USER_PAGE_ACTIONS}`}>
        User Actions
      </ContentLink>
      .
    </Paragraph>
  </>
);

export default UserPageNotesListRecentlyDeleted;
