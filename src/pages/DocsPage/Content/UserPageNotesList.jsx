import React from 'react';

import { LazyImage } from 'Components';

import notesListImg from 'Assets/docs/user-page-notes-list.png';

import { ImageContainer, Paragraph } from './styledComponents';

const UserPageNotesList = () => (
  <>
    <Paragraph>
      Notes list provides user ability to manage notes. Available all CRUD
      operations including filter by note name field. Each note shows the date
      of creation or update, lock status and content preview.
    </Paragraph>
    <ImageContainer width={200}>
      <LazyImage src={notesListImg} alt="Folders List" />
    </ImageContainer>
  </>
);

export default UserPageNotesList;
