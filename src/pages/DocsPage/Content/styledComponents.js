import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const ImageContainer = styled.div`
  border: 1px solid var(--swiss-coffee);
  width: ${({ width }) => `${width}px`};
  margin: 0 auto 20px;
`;

export const InlineImageContainer = styled.div`
  border: 1px solid var(--swiss-coffee);
  width: ${({ width }) => `${width}px`};
`;

export const LiContent = styled.p`
  letter-spacing: 0.2px;
  margin-bottom: 20px;
`;

export const Paragraph = styled.p`
  letter-spacing: 0.2px;
  text-indent: 20px;
  margin-bottom: 20px;
`;

export const ContentLink = styled(Link)`
  line-height: 1;
  font-weight: 500;
  color: var(--blue-zodiac);

  &:hover {
    color: var(--blue-zodiac);
    border-bottom: 1px solid var(--blue-zodiac);
  }
`;

export const DiskUL = styled.ul`
  padding-left: 70px;
  list-style: disc;
`;

export const ULli = styled.li`
  margin-bottom: 20px;
`;
