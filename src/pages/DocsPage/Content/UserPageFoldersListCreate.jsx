import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import newFolderButtonImg from 'Assets/docs/new-folder-button.png';
import newFolderEditModeImg from 'Assets/docs/new-folder-edit-mode.png';
import newFolderSavedImg from 'Assets/docs/new-folder-saved.png';

import { ImageContainer, LiContent, Paragraph } from './styledComponents';

const UserPageFoldersListCreate = () => (
  <>
    <Paragraph>To create the folder follow these steps:</Paragraph>
    <UserPageFoldersListCreate.OL>
      <li>
        <LiContent>Press the button &#xab;New Folder&#xbb;.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={newFolderButtonImg} alt="Create Folder Button" />
        </ImageContainer>
      </li>
      <li>
        <LiContent>Enter the unique folder name in user context.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={newFolderEditModeImg} alt="Edit Folder Mode" />
        </ImageContainer>
      </li>
      <li>
        <LiContent>Press Enter or change focus to save the folder.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={newFolderSavedImg} alt="Folder Saved" />
        </ImageContainer>
      </li>
    </UserPageFoldersListCreate.OL>
  </>
);

UserPageFoldersListCreate.OL = styled.ol`
  padding-left: 70px;
`;

export default UserPageFoldersListCreate;
