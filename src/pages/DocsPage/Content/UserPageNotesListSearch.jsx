import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import searchNotes from 'Assets/docs/notes-search.png';
import searchedNotes from 'Assets/docs/notes-searched.png';

import { ImageContainer, LiContent, Paragraph } from './styledComponents';

const UserPageNotesListSearch = () => (
  <>
    <Paragraph>To search the notes follow these steps:</Paragraph>
    <UserPageNotesListSearch.OL>
      <li>
        <LiContent>Enter text for search.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={searchNotes} alt="Search Notes" />
        </ImageContainer>
      </li>
      <li>
        <LiContent>Press enter key or &#xab;search&#xbb; icon.</LiContent>
        <ImageContainer width={200}>
          <LazyImage src={searchedNotes} alt="Searched Notes" />
        </ImageContainer>
      </li>
    </UserPageNotesListSearch.OL>
  </>
);

UserPageNotesListSearch.OL = styled.ol`
  padding-left: 70px;
`;

export default UserPageNotesListSearch;
