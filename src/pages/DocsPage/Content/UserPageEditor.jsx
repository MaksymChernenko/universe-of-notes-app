import React from 'react';

import { LazyImage } from 'Components';

import userPageEditorImg from 'Assets/docs/user-page-editor.png';

import { ImageContainer, Paragraph } from './styledComponents';

const UserPageEditor = () => (
  <>
    <Paragraph>
      Editor allows the user to create and maintain notes. It provides a wide
      range of possibilities by providing a selection of actions & plugins.It
      also allows conveniently keep track of the date and time & displays
      information about the number of characters, words and lines.
    </Paragraph>
    <ImageContainer width={900}>
      <LazyImage src={userPageEditorImg} alt="Editor" />
    </ImageContainer>
  </>
);

export default UserPageEditor;
