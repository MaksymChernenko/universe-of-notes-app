import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import editNoteButton from 'Assets/docs/edit-note-button.png';
import noteEditMode from 'Assets/docs/note-edit-mode.png';
import noteSaved from 'Assets/docs/note-saved.png';

import { ImageContainer, LiContent, Paragraph } from './styledComponents';

const UserPageNotesListEdit = () => (
  <>
    <Paragraph>There are two ways to edit note:</Paragraph>
    <UserPageNotesListEdit.UL>
      <li>
        <UserPageNotesListEdit.OL>
          <li>
            <LiContent>
              Click the right mouse button on the note list to display a context
              menu.
            </LiContent>
            <ImageContainer width={240}>
              <LazyImage src={editNoteButton} alt="Edit Note Button" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>Press &#xab;Edit&#xbb; button.</LiContent>
          </li>
          <li>
            <LiContent>Enter the unique note name in folder context.</LiContent>
            <ImageContainer width={200}>
              <LazyImage src={noteEditMode} alt="Edit Note Mode" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>Press Enter or change focus to save the note.</LiContent>
            <ImageContainer width={200}>
              <LazyImage src={noteSaved} alt="Note Saved" />
            </ImageContainer>
          </li>
        </UserPageNotesListEdit.OL>
      </li>
      <li>
        <LiContent>
          Press enter key on the keyboard and repeat steps 3 & 4.
        </LiContent>
      </li>
    </UserPageNotesListEdit.UL>
  </>
);

UserPageNotesListEdit.OL = styled.ol`
  padding-left: 30px;
`;

UserPageNotesListEdit.UL = styled.ul`
  padding-left: 70px;
  list-style: disc;
`;

export default UserPageNotesListEdit;
