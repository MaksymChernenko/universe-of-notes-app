import * as R from 'ramda';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';

import { fetchUserNotes, fetchLikes, updateNoteRating } from 'Actions/notes';
import { getUserNotes, getLikes } from 'Selectors/notes';

import { FETCHING_STATE } from 'Services/reduxHelpers';
import { getSimpleMenuItems, USER_DETAILED_VIEW_MODES } from 'Utils';

import { MainDivider } from 'Pages/MainPage/styledComponents';
import Header from './Header';
import NoteViewer from './NoteViewer';
import NoteViewerModal from './NoteViewerModal';
import UserNotesList from './UserNotesList';

const UserDetailsPage = () => {
  const dispatch = useDispatch();
  const { username } = useParams();

  const userNotesData = useSelector(getUserNotes);
  const likes = useSelector(getLikes);

  const [activeNote, setActiveNote] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [isFilter, setIsFilter] = useState(false);
  const [updateRatingIdList, setUpdateRatingIdList] = useState([]);
  const [openNoteViewer, setOpenNoteViewer] = useState(false);
  const [search, setSearch] = useState('');
  const [viewMode, setViewMode] = useState(
    USER_DETAILED_VIEW_MODES.COLUMN_MODE,
  );
  const [userNotes, setUserNotes] = useState([]);
  const [userLikes, setUserLikes] = useState([]);

  const isNotesFetching = userNotesData.state === FETCHING_STATE.FETCHING;
  const isNotesLoaded = userNotesData.state === FETCHING_STATE.LOADED;

  useEffect(() => {
    if (isNotesFetching) {
      setActiveNote(null);
    }
  }, [isNotesFetching]);

  useEffect(() => {
    dispatch(
      fetchUserNotes({
        author: username,
        page: 1,
      }),
    );
    dispatch(fetchLikes());
  }, [username]);

  useEffect(() => {
    if (isNotesLoaded) {
      setUserNotes(getSimpleMenuItems(userNotesData.data));
    }
  }, [userNotesData]);

  useEffect(() => {
    if (likes.state === FETCHING_STATE.LOADED) {
      setUserLikes(likes.data);
    }
  }, [likes]);

  const searchHandler = () => {
    const payload = {
      author: username,
      page: 1,
    };
    if (!search) {
      setIsFilter(false);
    } else {
      setIsFilter(true);
      payload.name = search;
    }
    setCurrentPage(1);
    dispatch(fetchUserNotes(payload));
  };

  const setColumnViewMode = () =>
    setViewMode(USER_DETAILED_VIEW_MODES.COLUMN_MODE);

  const setPage = page => {
    const payload = {
      author: username,
      page,
    };
    if (isFilter) {
      payload.name = search;
    }
    dispatch(fetchUserNotes(payload));
  };

  const setRowViewMode = () => setViewMode(USER_DETAILED_VIEW_MODES.ROW_MODE);

  const updateUserRating = (noteId, rating) =>
    setUserNotes(prev =>
      prev.map(item =>
        item.id === noteId ? R.mergeDeepRight(item, { rating }) : item,
      ),
    );

  const addNoteIdToUpdateRating = noteId =>
    setUpdateRatingIdList(prev => [...prev, noteId]);

  const removeNoteFromUpdateRating = noteId =>
    setUpdateRatingIdList(prev => prev.filter(id => id !== noteId));

  const setRating = (noteId, isLiked) => {
    addNoteIdToUpdateRating(noteId);
    dispatch(
      updateNoteRating({
        noteId,
        payload: {
          rating: isLiked ? 1 : 0,
        },
        onSuccess: ({ data }) => {
          const { rating } = data;
          dispatch(
            fetchLikes({
              onSuccess: () => {
                updateUserRating(noteId, rating);
                removeNoteFromUpdateRating(noteId);
              },
              onError: () => removeNoteFromUpdateRating(noteId),
            }),
          );
        },
        onError: () => removeNoteFromUpdateRating(noteId),
      }),
    );
  };

  return (
    <UserDetailsPage.Wrapper>
      <Header
        setColumnViewMode={setColumnViewMode}
        setRowViewMode={setRowViewMode}
        username={username}
        search={search}
        setSearch={setSearch}
        searchHandler={searchHandler}
        isNotesFetching={isNotesFetching}
      />
      <UserDetailsPage.Content viewmode={viewMode}>
        <UserNotesList
          data={userNotes}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          setActiveNote={setActiveNote}
          totalCount={userNotesData.count}
          viewMode={viewMode}
          setOpenNoteViewer={setOpenNoteViewer}
          isNotesFetching={isNotesFetching}
          isNotesLoaded={isNotesLoaded}
          setPage={setPage}
          likes={userLikes}
          updateRating={setRating}
          updateRatingIdList={updateRatingIdList}
        />
        {viewMode === USER_DETAILED_VIEW_MODES.ROW_MODE && (
          <NoteViewerModal
            activeNote={activeNote}
            open={openNoteViewer}
            setOpen={setOpenNoteViewer}
            viewMode={viewMode}
          />
        )}
        {viewMode === USER_DETAILED_VIEW_MODES.COLUMN_MODE && (
          <>
            <MainDivider />
            <NoteViewer activeNote={activeNote} viewMode={viewMode} />
          </>
        )}
      </UserDetailsPage.Content>
    </UserDetailsPage.Wrapper>
  );
};

UserDetailsPage.Content = styled.div`
  padding: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '0' : '10px 0'};
  display: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? 'block' : 'flex'};
  height: calc(100% - 48px);
`;

UserDetailsPage.Wrapper = styled.div`
  height: calc(100vh - 133px);
`;

export default UserDetailsPage;
