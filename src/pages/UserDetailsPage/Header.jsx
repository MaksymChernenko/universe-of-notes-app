import PropTypes from 'prop-types';

import { Button, Tooltip } from 'antd';
import { PicCenterOutlined, PicLeftOutlined } from '@ant-design/icons';

import React from 'react';
import styled from 'styled-components';

import { applyBlur } from 'Utils';

import { Search, SearchContainer } from 'Pages/UserPage/styledComponents';

const Header = ({
  isNotesFetching,
  search,
  setSearch,
  searchHandler,
  setColumnViewMode,
  setRowViewMode,
  username,
}) => {
  const setColumnViewModeHandler = e => {
    applyBlur(e);
    setColumnViewMode();
  };

  const setRowViewModeHandler = e => {
    applyBlur(e);
    setRowViewMode();
  };

  return (
    <Header.Wrapper>
      <Header.Grouper>
        <Header.Title>User Detailed</Header.Title>
        <SearchContainer style={{ margin: 0, width: 240 }}>
          <Search
            size="small"
            placeholder="Filter by name"
            value={search}
            onChange={({ target: { value } }) => setSearch(value)}
            onSearch={searchHandler}
            disabled={isNotesFetching}
          />
        </SearchContainer>
      </Header.Grouper>
      <Header.Content>
        <Header.Actions>
          <Tooltip
            mouseEnterDelay={1}
            placement="bottomRight"
            title="Column view mode"
          >
            <Header.ActionButton
              onClick={setColumnViewModeHandler}
              size="large"
              icon={<PicLeftOutlined />}
            />
          </Tooltip>
          <Tooltip
            mouseEnterDelay={1}
            placement="bottomRight"
            title="Row view mode"
          >
            <Header.ActionButton
              onClick={setRowViewModeHandler}
              size="large"
              icon={<PicCenterOutlined />}
            />
          </Tooltip>
        </Header.Actions>
        <Header.Username>{username}</Header.Username>
      </Header.Content>
    </Header.Wrapper>
  );
};

Header.propTypes = {
  isNotesFetching: PropTypes.bool.isRequired,
  search: PropTypes.string.isRequired,
  searchHandler: PropTypes.func.isRequired,
  setColumnViewMode: PropTypes.func.isRequired,
  setRowViewMode: PropTypes.func.isRequired,
  setSearch: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
};

Header.Actions = styled.div`
  margin-right: 100px;
  width: 90px;
  display: flex;
  justify-content: space-between;

  & .ant-btn:hover,
  & .ant-btn:focus {
    background-color: inherit !important;
    color: var(--blue-zodiac) !important;
    border: none !important;
  }
`;

Header.ActionButton = styled(Button)`
  background-color: inherit;
  border: none;
`;

Header.Content = styled.div`
  display: flex;
  align-items: center;
  padding: 4px 40px 4px 0;
`;

Header.Grouper = styled.div`
  display: flex;
  align-items: center;
`;

Header.Title = styled.h2`
  text-transform: uppercase;
  padding-left: 40px;
  font-size: 32px;
  line-height: 1;
  letter-spacing: 1px;
  color: var(--blue-zodiac);
  opacity: 0.4;
  margin-right: 40px;
`;

Header.Username = styled.p`
  font-size: 18px;
  font-style: italic;
  color: var(--clam-shell);
  filter: brightness(0.4);
`;

Header.Wrapper = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: var(--swiss-coffee);
`;

export default Header;
