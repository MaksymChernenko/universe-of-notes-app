import { Input } from 'antd';
import {
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
  LockOutlined,
  LogoutOutlined,
  MergeCellsOutlined,
  SettingOutlined,
} from '@ant-design/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import styled from 'styled-components';

import { toggleFoldersList } from 'Actions/global';
import { logout } from 'Actions/session';
import { getSelectedFolder, isFoldersDisabled } from 'Selectors/folders';
import { getNotes, getSelectedNote, isNotesDisabled } from 'Selectors/notes';

import { routePaths } from 'Routes';
import { FETCHING_STATE } from 'Services/reduxHelpers';
import {
  ACTIVE_PAGES,
  applyBlur,
  NOTIFICATION_TYPES,
  openNotificationWithIcon,
} from 'Utils';

import { ActionItem, UserSettingsModal } from 'Components';

const Header = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const [activePage, setActivePage] = useState('');
  const [openUserSettings, setOpenUserSettings] = useState(false);

  const notes = useSelector(getNotes);
  const selectedFolder = useSelector(getSelectedFolder);
  const selectedNote = useSelector(getSelectedNote);
  const isFolderActionsDisabled = useSelector(isFoldersDisabled);
  const isNoteActionsDisabled = useSelector(isNotesDisabled);

  useEffect(() => {
    switch (location.pathname) {
      case routePaths.main:
        setActivePage(ACTIVE_PAGES.MAIN_PAGE);
        break;
      case routePaths.user:
        setActivePage(ACTIVE_PAGES.USER_PAGE);
        break;
      default:
        setActivePage('');
    }
  }, [location]);

  const searchHandler = (value, e) => {
    applyBlur(e);
  };

  const createNoteHandler = () => {
    selectedNote.createNote();
  };

  const deleteSelectedItemHandler = e => {
    if (selectedFolder.deletable) {
      const { id } = selectedFolder;
      selectedFolder.deleteFolder(id);
    }
    if (selectedNote.deletable) {
      const { id } = selectedNote;
      selectedNote.deleteNote(id);
    }
    applyBlur(e);
  };

  const toggleFoldersListHandler = e => {
    dispatch(toggleFoldersList());
    applyBlur(e);
  };

  const logoutHandler = e => {
    dispatch(
      logout({
        onError: error => {
          const { data } = error;
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: data.data,
          });
        },
      }),
    );
    applyBlur(e);
  };

  const lockNoteHandler = e => {
    const { id, locked } = selectedNote;
    selectedNote.lockNote(id, locked);
    applyBlur(e);
  };

  const deleteAllNotesHandler = () => {
    selectedNote.deleteAllInactiveNotes();
  };

  const openUserSettingsHandler = e => {
    setOpenUserSettings(true);
    applyBlur(e);
  };

  return (
    <Header.Wrapper>
      <Header.ActionList>
        {activePage === ACTIVE_PAGES.USER_PAGE && (
          <>
            <Header.ActionLi>
              <ActionItem
                title="Toggle folders visibility"
                disabled={isFolderActionsDisabled || isNoteActionsDisabled}
                icon={<MergeCellsOutlined />}
                onClick={toggleFoldersListHandler}
              />
            </Header.ActionLi>
            <Header.ActionLi>
              <ActionItem
                title="Delete item"
                disabled={
                  (!selectedFolder.deletable && !selectedNote.deletable) ||
                  isFolderActionsDisabled ||
                  isNoteActionsDisabled ||
                  selectedFolder.name === 'Recently Deleted'
                }
                icon={<CloseOutlined />}
                onClick={deleteSelectedItemHandler}
              />
            </Header.ActionLi>
            <Header.ActionLi>
              <ActionItem
                disabled={
                  !selectedFolder.id ||
                  selectedFolder.name === 'Recently Deleted' ||
                  isNoteActionsDisabled ||
                  isFolderActionsDisabled
                }
                title="Create note"
                icon={<EditOutlined />}
                onClick={createNoteHandler}
              />
            </Header.ActionLi>
            <Header.ActionLi>
              <ActionItem
                disabled={
                  !selectedNote.id ||
                  selectedFolder.name === 'Recently Deleted' ||
                  isNoteActionsDisabled
                }
                title="Lock note"
                icon={<LockOutlined />}
                onClick={lockNoteHandler}
              />
            </Header.ActionLi>
            {selectedFolder.name === 'Recently Deleted' && (
              <Header.ActionLi>
                <ActionItem
                  title="Delete all notes"
                  disabled={notes.state !== FETCHING_STATE.LOADED}
                  icon={<DeleteOutlined />}
                  onClick={deleteAllNotesHandler}
                />
              </Header.ActionLi>
            )}
          </>
        )}
      </Header.ActionList>
      <div>
        {activePage === ACTIVE_PAGES.MAIN_PAGE && (
          <Header.Search
            placeholder="Search"
            enterButton
            onSearch={searchHandler}
          />
        )}
        {activePage === ACTIVE_PAGES.USER_PAGE && (
          <>
            <ActionItem
              style={{ marginRight: 20 }}
              title="User Settings"
              icon={<SettingOutlined />}
              onClick={openUserSettingsHandler}
            />
            <UserSettingsModal
              open={openUserSettings}
              setOpen={setOpenUserSettings}
            />
          </>
        )}
        <ActionItem
          title="Logout"
          icon={<LogoutOutlined />}
          onClick={logoutHandler}
        />
      </div>
    </Header.Wrapper>
  );
};

Header.ActionLi = styled.li`
  margin-right: 30px;
`;

Header.ActionList = styled.ul`
  display: flex;
`;

Header.Search = styled(Input.Search)`
  width: 200px;
  margin-right: 20px;

  & .ant-btn {
    background-color: var(--blue-zodiac);
    border: none;
  }

  & .ant-btn,
  & .ant-input-group-addon {
    border-radius: 0 10px 10px 0;
  }

  & .ant-input {
    border-radius: 10px 0 0 10px;
  }

  & .ant-btn-primary:hover,
  & .ant-btn-primary:focus {
    background-color: var(--ebb) !important;
    color: var(--blue-zodiac) !important;
    border: none !important;
  }

  & .ant-input:hover,
  & .ant-input:focus {
    box-shadow: none;
    border: 1px solid var(--blue-zodiac);
  }
`;

Header.Wrapper = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 40px;
  background-color: var(--swiss-coffee);
  border-bottom: 1px solid var(--hurricane);

  & .ant-btn:hover,
  & .ant-btn:focus {
    background-color: unset;
    color: unset;
    border: 1px solid var(--hurricane);
  }
`;

export default Header;
