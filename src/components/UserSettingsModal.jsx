import PropTypes from 'prop-types';

import { Button, Divider, Modal, Input } from 'antd';
import { KeyOutlined, MailOutlined, UserOutlined } from '@ant-design/icons';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { updateUser } from 'Actions/session';
import { getUser } from 'Selectors/session';

import { NOTIFICATION_TYPES, openNotificationWithIcon } from 'Utils';

import { Form, SubmitButton } from 'Pages/SignPage/styledComponents';

import SettingsImg from 'Assets/settings.png';

const initialUserData = {
  username: '',
  email: '',
  password: '',
  newPassword: '',
};

const UserSettingsModal = ({ open, setOpen }) => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [userData, setUserData] = useState(initialUserData);

  const user = useSelector(getUser);

  useEffect(() => {
    if (open) {
      setUserData({
        username: user.username,
        email: user.email,
      });
    }
  }, [user, open]);

  const changeHandler = ({ target: { name, value } }) => {
    setUserData(prevData => ({
      ...prevData,
      [name]: value,
    }));
  };

  const closeHandler = () => {
    setOpen(false);
    setUserData(initialUserData);
  };

  const saveUserSettingsHandler = e => {
    const target = e.currentTarget;
    const { username, email, password, newPassword } = userData;
    let payload = {};

    if (username !== user.username) {
      payload = { username };
    }

    if (email !== user.email) {
      payload = { ...payload, email };
    }

    if (password && newPassword) {
      payload = { ...payload, password, newPassword };
    }

    if (Object.keys(payload).length === 0) {
      openNotificationWithIcon({
        type: NOTIFICATION_TYPES.WARNING,
        message: `Warning`,
        description: 'There is nothing to update',
      });
      target.blur();
      return;
    }

    setLoading(true);
    dispatch(
      updateUser({
        payload,
        onSuccess: () => {
          setLoading(false);
          target.blur();
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.SUCCESS,
            message: `Success`,
            description: 'User updated',
          });
        },
        onError: () => {
          setLoading(false);
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: 'Update user error',
          });
        },
      }),
    );
  };

  return (
    <Modal
      width={600}
      visible={open}
      title="User Settings"
      onOk={saveUserSettingsHandler}
      onCancel={closeHandler}
      footer={[
        <UserSettingsModal.CancelButton key="cancel" onClick={closeHandler}>
          Cancel
        </UserSettingsModal.CancelButton>,
        <SubmitButton
          key="submit"
          loading={loading}
          onClick={saveUserSettingsHandler}
        >
          Save
        </SubmitButton>,
      ]}
    >
      <UserSettingsModal.Wrapper>
        <Form
          style={{
            width: 280,
          }}
        >
          <UserSettingsModal.Divider orientation="center">
            Change user data
          </UserSettingsModal.Divider>
          <Input
            autoFocus
            required
            autoComplete="off"
            name="username"
            value={userData.username}
            onChange={changeHandler}
            style={{ marginBottom: 20, width: 280 }}
            allowClear
            placeholder="Enter username"
            prefix={<UserOutlined />}
          />
          <Input
            type="email"
            required
            autoComplete="off"
            name="email"
            value={userData.email}
            onChange={changeHandler}
            style={{ marginBottom: 20, width: 280 }}
            allowClear
            placeholder="Enter email"
            prefix={<MailOutlined />}
          />
          <UserSettingsModal.Divider orientation="center">
            Change user password
          </UserSettingsModal.Divider>
          <Input.Password
            type="password"
            autoComplete="off"
            name="password"
            value={userData.password}
            onChange={changeHandler}
            style={{ marginBottom: 20, width: 280 }}
            placeholder="Enter current password"
            prefix={<KeyOutlined />}
          />
          <Input.Password
            type="password"
            autoComplete="off"
            name="newPassword"
            value={userData.newPassword}
            onChange={changeHandler}
            style={{ width: 280 }}
            placeholder="Enter new password"
            prefix={<KeyOutlined />}
          />
        </Form>
        <UserSettingsModal.Image src={SettingsImg} alt="settings" />
      </UserSettingsModal.Wrapper>
    </Modal>
  );
};

UserSettingsModal.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
};

UserSettingsModal.CancelButton = styled(Button)`
  &:focus,
  &:hover {
    color: var(--blue-zodiac);
    border-color: var(--swiss-coffee);
    box-shadow: 0 0 0 2px var(--swiss-coffee);
  }
`;

UserSettingsModal.Divider = styled(Divider)`
  margin: 0 0 20px !important;
  font-weight: 400 !important;
  color: var(--hurricane) !important;
`;

UserSettingsModal.Image = styled.img`
  width: 250px;
  filter: opacity(0.7);
`;

UserSettingsModal.Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 6px;
`;

export default UserSettingsModal;
