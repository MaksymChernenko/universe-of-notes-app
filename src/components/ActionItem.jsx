import PropTypes from 'prop-types';

import { Button, Tooltip } from 'antd';

import React from 'react';
import styled from 'styled-components';

const ActionItem = ({ title, style, shape = 'circle', ...props }) => (
  <Tooltip mouseEnterDelay={1} placement="bottomRight" title={title}>
    <ActionItem.ActionButton style={style} shape={shape} {...props} />
  </Tooltip>
);

ActionItem.defaultProps = { shape: 'circle', style: {} };

ActionItem.propTypes = {
  shape: PropTypes.string,
  style: PropTypes.object,
  title: PropTypes.string.isRequired,
};

ActionItem.ActionButton = styled(Button)`
  background-color: var(--blue-zodiac);
  color: var(--merino);
  border: 1px solid var(--merino);
`;

export default ActionItem;
