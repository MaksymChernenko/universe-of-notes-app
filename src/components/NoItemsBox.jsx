import PropTypes from 'prop-types';

import React from 'react';
import styled from 'styled-components';

const NoItemsBox = ({ text }) => (
  <NoItemsBox.Wrapper>
    <NoItemsBox.Content>{text}</NoItemsBox.Content>
  </NoItemsBox.Wrapper>
);

NoItemsBox.defaultProps = {
  text: 'No items',
};

NoItemsBox.propTypes = {
  text: PropTypes.string,
};

NoItemsBox.Wrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

NoItemsBox.Content = styled.p`
  padding: 10px 20px;
  border: 1px solid var(--soft-amber);
  border-radius: 10px;
`;

export default NoItemsBox;
