import 'react-lazy-load-image-component/src/effects/blur.css';

import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';

const LazyImage = props => (
  <LazyLoadImage width="100%" effect="blur" {...props} />
);

export default LazyImage;
